
# Ludwick API Service

## Build Instructions

```
python3 -m virtualenv .
source bin/activate
pip3 install -r requirements.txt
```

## Run Server

```
python manage.py runserver -h 0.0.0.0 -p 80
```
