
from flask import Flask, request, jsonify
import uuid
import random

app = Flask(__name__)

@app.route("/api/login", methods = ["POST"])
def api_login():
    username = request.json.get('username')
    password = request.json.get('password')
    
    return jsonify({
        "code": 200,
        "api_token": str(uuid.uuid4())
    })

@app.route("/api/register", methods = ["POST"])
def api_register():
    username = request.json.get('username')
    password = request.json.get('password')
    
    return jsonify({
        "code": 200,
        "api_token": str(uuid.uuid4())
    })

@app.route("/api/wallet", methods = ["POST"])
def api_wallet():
    api_token = request.json.get('api_token')
    
    return jsonify({
        "code": 200,
        "wallet_hash": uuid.uuid4(),
        "balance": random.random() * random.randint(1, 10000)
    })

@app.route("/api/send_money", methods = ["POST"])
def api_send_money():
    api_token = request.json.get('api_token')
    other_wallet_hash = request.json.get('other_wallet_hash')
    amount = request.json.get("amount")
    
    return jsonify({
        "code": 200
    })
